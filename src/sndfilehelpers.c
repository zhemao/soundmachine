#include "sndfilehelpers.h"
#include <stdlib.h>
#include <sndfile.h>
#include <string.h>
#include <limits.h>
#include "synth.h"

int get_format(const char * filename){
	char * ext = strrchr(filename, '.') + 1;
	
	if(strcmp(ext, "wav")==0) return SF_FORMAT_WAV | SF_FORMAT_PCM_16;
	if(strcmp(ext, "aiff")==0) return SF_FORMAT_AIFF | SF_FORMAT_PCM_16;
	if(strcmp(ext, "au")==0) return SF_FORMAT_AU | SF_FORMAT_PCM_16;
	if(strcmp(ext, "flac")==0) return SF_FORMAT_FLAC | SF_FORMAT_DOUBLE;
	if(strcmp(ext, "ogg")==0) return SF_FORMAT_OGG | SF_FORMAT_DOUBLE;
	
	return -1;
}

double * read_sndfile(const char * filename, int * size){
	SNDFILE * sf;
	SF_INFO info;
	double * sound;
	int i;
	
	memset(&info, 0, sizeof(info));
	
	sf = sf_open(filename, SFM_READ, &info);
	
	if(sf == NULL) return NULL;
	
	*size = info.frames * info.channels;
	sound = (double*)calloc(sizeof(double), *size);
	
	sf_readf_double(sf, sound, info.frames);
	
	sf_close(sf);
	
	return sound;
}

int write_sndfile(const char * filename, double * data, int n){
	SF_INFO info;
	SNDFILE * sf;
	int i;
	int res;
	int format;
	
	format = get_format(filename);
	if(format < 0) return 0;
	
	memset(&info, 0, sizeof(info));
	info.format = format | SF_ENDIAN_CPU;
	info.channels = synth_state.channels;
	info.samplerate = synth_state.samp_rate;
	info.frames = (double)n / (double)synth_state.channels;
	
	sf = sf_open(filename, SFM_WRITE, &info);
	
	if(sf == NULL) return 0;
	
	res = sf_write_double(sf, data, n);
	
	sf_write_sync(sf);
	sf_close(sf);
	
	return res == n;
}
