#ifndef __SNDMACHINE_WAVEFORM_H__
#define __SNDMACHINE_WAVEFORM_H__

typedef double (*waveform)(double, double, double);

double pure_sine(double, double, double);
double second_harmonic(double, double, double);
double third_harmonic(double, double, double);
double clipped_wave(double, double, double);
waveform string_to_wf(char * str);
waveform cycle_waveform();

#endif /* __WAVEFORM_H__ */

