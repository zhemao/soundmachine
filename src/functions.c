#include "functions.h"
#include "synth.h"
#include "waveforms.h"
#include "sndfilehelpers.h"
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

void on_error (lua_State *L, const char *fmt, ...) {
	va_list argp;
	va_start(argp, fmt);
	vfprintf(stderr, fmt, argp);
	va_end(argp);
	if(lua_isstring(L, -1) && !lua_isnumber(L, -1)){
		const char * errmess = lua_tostring(L, -1);
		fprintf(stderr, "%s\n", errmess);
	}
	if(exit_on_error){
		lua_close(L);
		if(synth_state.device != NULL) 
			ao_close(synth_state.device);
		ao_shutdown();
		exit(EXIT_FAILURE);
	}
}

void add_number(lua_State * L, const char * name, double d){
	lua_pushnumber(L, d);
	lua_setglobal(L, name);
}

void add_integer(lua_State * L, const char * name, long i){
	lua_pushinteger(L, i);
	lua_setglobal(L, name);
}

void add_string(lua_State * L, const char * name, char * str){
	lua_pushstring(L, str);
	lua_setglobal(L, name);
}

void add_cfunction(lua_State * L, const char * name, lua_CFunction func){
	lua_pushcfunction(L, func);
	lua_setglobal(L, name);
}

const char * get_string_field(lua_State * L, int ind, const char * key){
	char * result;
	lua_pushstring(L, key);
	lua_gettable(L, ind);
	if(!lua_isstring(L, -1)){
		on_error(L, "the %s field is not a string!\n", key); 
		return NULL;
	}
	result = lua_tostring(L, -1);
	lua_pop(L, 1);
	return result;
}

double get_number_field(lua_State * L, int ind, const char * key){
	double result;
	lua_pushstring(L, key);
	lua_gettable(L, ind);
	if(!lua_isnumber(L, -1)){
		on_error(L, "the %s field is not a number!\n", key); 
		return 0;
	}
	result = lua_tonumber(L, -1);
	lua_pop(L, 1);
	return result;
}

short * get_integer_array(lua_State * L, int ind){
	int n,i;
	short * array;
	
	n = luaL_getn(L, 1);
	array = (short*)calloc(sizeof(short), n);
	
	for(i=1;i<=n;i++){
		lua_rawgeti(L, ind, i);
		array[i-1] = lua_tointeger(L, -1);
		lua_pop(L, 1);
	}
	
	return array;
}

double * get_number_array(lua_State * L, int ind){
	int n,i;
	double * array;
	
	n = luaL_getn(L, 1);
	array = (double*)calloc(sizeof(double), n);
	
	for(i=1;i<=n;i++){
		lua_rawgeti(L, ind, i);
		array[i-1] = lua_tonumber(L, -1);
		lua_pop(L, 1);
	}
	
	return array;
}

void push_integer_array(lua_State * L, short * array, int n){
	int i, t;
	
	lua_createtable(L, n, 0);
	t = lua_gettop(L);
	
	for(i=1; i<=n; i++){
		lua_pushinteger(L, array[i-1]);
		lua_rawseti(L, t, i);
	}
	
	lua_settop(L, t);
}

void push_number_array(lua_State * L, double * array, int n){
	int i, t;
	
	lua_createtable(L, n, 0);
	t = lua_gettop(L);
	
	for(i=1; i<=n; i++){
		lua_pushnumber(L, array[i-1]);
		lua_rawseti(L, t, i);
	}
	
	lua_settop(L, t);
}

int lua_set_waveform(lua_State * L){
	if(lua_gettop(L) != 1){
		on_error(L, "set_waveform takes 1 argument\n");
		return 0;
	}
	if(!lua_isstring(L, 1)){
		on_error(L, "argument 1 of set_waveform must be a string\n");
		return 0;
	}
	const char * wavestr = lua_tostring(L, 1);
	synth_state.wf = string_to_wf(wavestr);
	add_string(globalState, "waveform", wavestr);
	return 0;
}

int lua_set_tempo(lua_State * L){
	if(lua_gettop(L) != 1){ 
		on_error(L, "set_tempo takes 1 argument\n"); 
		return 0;
	}
	if(!lua_isnumber(L, 1)){
		on_error(L, "argument 1 of set_tempo must be a number\n");
		return 0;
	}
	synth_state.tempo = lua_tonumber(L, 1);
	add_number(globalState, "tempo", synth_state.tempo);
	return 0;
}

int lua_set_amplitude(lua_State * L){
	if(lua_gettop(L) != 1){ 
		on_error(L, "set_amplitude takes 1 argument\n");
		return 0;
	}
	if(!lua_isnumber(L, 1)){
		on_error(L, "argument 1 of set_amplitude must be a number\n");
		return 0;
	}
	synth_state.amp = lua_tonumber(L, 1);
	add_number(globalState, "amplitude", synth_state.amp);
	return 0;
}

int lua_reset_device(lua_State * L){
	int numargs = lua_gettop(L);
	char * driver_name = "default";
	char * filename = NULL;
	int driver;
	int channels = DEFAULT_CHANNELS;
	long samp_rate = DEFAULT_SAMP_RATE;
	
	if(numargs > 0){
		if(!lua_isstring(L, 1)){
			on_error(L, "Argument 1 of reset_device must be a string\n");
			return 0;
		}
		driver_name = lua_tostring(L, 1);
	}
	if(numargs > 1){
		if(!lua_isnumber(L, 2)){
			on_error(L, "Argument 2 of reset_device must be integer\n");
			return 0;
		}
		channels = lua_tointeger(L, 2);
	}
	if(numargs > 2){
		if(!lua_isnumber(L, 3)){
			on_error(L, "Argument 3 of reset_device must be integer\n");
			return 0;
		}
		samp_rate = lua_tointeger(L, 3);
	}
	if(numargs > 3){
		if(!lua_isstring(L, 4)){
			on_error(L, "Argument 4 of reset_device must be string\n");
			return 0;
		}
		filename = lua_tostring(L, 4);
	}
		
	if(strcmp(driver_name, "default")==0){
		driver = ao_default_driver_id();
		if(driver == -1){
			on_error(L, "No default driver\n");
			return 0;
		}
	}
	else {
		driver = ao_driver_id(driver_name);
		if(driver == -1){
			on_error(L, "No driver named '%s'\n", driver_name);
			return 0;
		}
	}
	
	reset_device(driver, channels, samp_rate, filename);
	
	add_string(L, "driver", driver_name);
	add_integer(L, "channels", channels);
	add_integer(L, "samp_rate", samp_rate);
	
	return 0;
}

int lua_play(lua_State * L){
	int numargs = lua_gettop(L);
	int i;
	
	for(i=1; i<=numargs; i++){
		if(lua_isstring(L, i) && !lua_isnumber(L, i)){
			const char * notestr = lua_tostring(L, i);
			int len = lua_strlen(L, i);
			char temp[len+1];
			strcpy(temp, notestr);
			if(!play_notestr(temp)){
				on_error(L, "argument %d is not a valid note description\n", i);
				return 0;
			}
		} 
		else if(lua_istable(L, i)){
			const char * note = get_string_field(L, i, "note");
			int octave = (int)get_number_field(L, i, "octave");
			double duration = get_number_field(L, i, "beats");
			if(!play_note(note, octave, duration)){
				on_error(L, "argument %d is not a valid note description\n", i);
				return 0;
			}
		}
		else{ 
			on_error(L, "argument %d to play() is not a valid type\n", i);
			return 0;
		}
	}
	return 0;
}

int lua_rest(lua_State * L){
	double beats;
	if(lua_gettop(L) != 1){ 
		on_error(L, "rest takes 1 argument");
		return 0;
	}
	if(!lua_isnumber(L, 1)){
		on_error(L, "argument 1 of rest must be a number\n");
		return 0;
	}
	beats = lua_tonumber(L, 1);
	beat_delay(beats);
}

int lua_play_freq(lua_State * L){
	int i;
	double freq;
	double secs;
	if(lua_gettop(L) != 2){
		on_error(L, "play_freq takes 2 arguments\n");
		return 0;
	}
	for(i=1; i<=2; i++){
		if(!lua_isnumber(L, i)){
			on_error(L, "Argument %d of play_freq must be a number\n", i);
			return 0;
		}
	}
	freq = lua_tonumber(L, 1);
	secs = lua_tonumber(L, 2);
	play_freq(freq, secs);
	
	return 0;
}

int lua_play_raw(lua_State * L){
	int n;
	short * buffer;
	
	if(lua_gettop(L) != 1){
		on_error(L, "play_raw takes 1 argument\n");
		return 0;
	}
	if(!lua_istable(L, 1)){
		on_error(L, "Argument 1 of play_raw must be a table\n");
		return 0;
	}
		
	n = luaL_getn(L, 1);
		
	buffer = get_integer_array(L, 1);
	ao_play(synth_state.device, (char*)buffer, n * sizeof(short));
	free(buffer);
	
	return 0;
}

int lua_play_channels(lua_State * L){
	int n;
	short * buffer;
	int i, j;
	if(lua_gettop(L) != synth_state.channels){
		on_error(L, "play_raw takes %d arguments\n", synth_state.channels);
		return 0;
	}
	if(!lua_istable(L, 1)){
		on_error(L, "Argument 1 of play_channels must be a table\n");
		return 0;
	}
	
	n = luaL_getn(L, 1);
	for(i=2; i<=synth_state.channels; i++){
		if(!lua_istable(L, i)){
			on_error(L, "Argument %d of play_channels must be a table\n", i);
			return 0;
		}
		if(luaL_getn(L, i)!=n){
			on_error(L, "length of arrays must match\n"); 
			return 0;
		}
	}
	
	buffer = (short*)calloc(sizeof(short), n * synth_state.channels);
	
	for(i=1; i<=n; i++){
		for(j=1; j<=synth_state.channels; j++){
			lua_rawgeti(L, j, i);
			buffer[(i-1)*synth_state.channels + j-1] = lua_tointeger(L, -1);
			lua_pop(L, 1);
		}
	}
	
	ao_play(synth_state.device, (char*)buffer, n * synth_state.channels * sizeof(short));
	free(buffer);
	return 0;
}

int lua_combine(lua_State * L){
	int channels;
	int i,j,n;
	double * buffer;
	
	channels = lua_gettop(L);
	if(channels < 2){
		on_error(L, "combine_channels takes at least 2 arguments\n");
		return 0;
	}
	if(!lua_istable(L, 1)){
		on_error(L, "Argument 1 of combine_channels must be a table\n");
		return 0;
	}
	
	n = luaL_getn(L, 1);
	for(i=2; i<=channels; i++){
		if(!lua_istable(L, i)){
			on_error(L, "Argument %d of combine_channels must be a table\n", i);
			return 0;
		}
		if(luaL_getn(L, i)!=n){
			on_error(L, "length of arrays must match\n"); 
			return 0;
		}
	}
	
	buffer = (double*)calloc(sizeof(double), n * channels);
	
	for(i=1; i<=n; i++){
		for(j=1; j<=channels; j++){
			lua_rawgeti(L, j, i);
			buffer[(i-1)*channels + j-1] = lua_tonumber(L, -1);
			lua_pop(L, 1);
		}
	}
	
	push_number_array(L, buffer, n * channels);
	free(buffer);
	
	return 1;
}

int lua_mix(lua_State * L){
	int channels;
	int i,j,n;
	double * buffer;
	
	channels = lua_gettop(L);
	if(channels < 2){
		on_error(L, "mix takes at least 2 arguments\n");
		return 0;
	}
	if(!lua_istable(L, 1)){
		on_error(L, "Argument 1 of mix must be a table\n");
		return 0;
	}
	
	n = luaL_getn(L, 1);
	for(i=2; i<=channels; i++){
		if(!lua_istable(L, i)){
			on_error(L, "Argument %d of mix must be a table\n", i);
			return 0;
		}
		if(luaL_getn(L, i)!=n){
			on_error(L, "length of arrays must match\n"); 
			return 0;
		}
	}
	
	buffer = (double*)calloc(sizeof(double), n);
	
	for(i=1; i<=n; i++){
		buffer[i-1] = 0;
		for(j=1; j<=channels; j++){
			lua_rawgeti(L, j, i);
			buffer[i-1] += lua_tonumber(L, -1);
			lua_pop(L, 1);
		}
		buffer[i-1]/=channels;
	}
	
	push_number_array(L, buffer, n);
	free(buffer);
	
	return 1;
}

int lua_subtract(lua_State * L){
	int i,n;
	double * buffer;
	
	if(lua_gettop(L) != 2){
		on_error(L, "subtract takes exactly 2 arguments\n");
		return 0;
	}
		
	for(i=1; i<=2; i++){
		if(!lua_istable(L, i)){
			on_error(L, "Argument %d of subtract must be a table\n");
			return 0;
		}
	}
	
	n = luaL_getn(L, 1);
	
	if(luaL_getn(L, 2) != n){
		on_error(L, "Array lengths must be equal\n");
		return 0;
	}
	
	buffer = (double*)calloc(sizeof(double), n);
	
	for(i=1; i<=n; i++){
		buffer[i-1] = 0;
		lua_rawgeti(L, 1, i);
		buffer[i-1] += lua_tonumber(L, -1);
		lua_pop(L, 1);
		lua_rawgeti(L, 2, i);
		buffer[i-1] -= lua_tonumber(L, -1);
		lua_pop(L, 1);
	}
	
	push_number_array(L, buffer, n);
	free(buffer);
	
	return 1;
}

int lua_amplify(lua_State * L){
	int i, n;
	double amp;
	double * array;
	
	if(lua_gettop(L) != 2){
		on_error(L, "amplify takes exactly 2 arguments");
		return 0;
	}
	if(!lua_istable(L, 1)){
		on_error(L, "argument 1 of amplify must be a table");
		return 0;
	}
	if(!lua_isnumber(L, 2)){
		on_error(L, "argument 2 of amplify must be a number");
		return 0;
	}
	
	n = luaL_getn(L, 1);
	
	amp = lua_tonumber(L, 2);
	array = get_number_array(L, 1);
	
	for(i=0; i<n; i++) array[i] *= amp;
	
	push_number_array(L, array, n);
	free(array);
	
	return 1;
}

int lua_separate(lua_State * L){
	double * channel_data;
	int raw_len, chan_len, channels, i, j;	
	
	if(lua_gettop(L) != 2){
		on_error(L, "separate takes two arguments\n");
		return 0;
	}
	if(!lua_istable(L, 1)){
		on_error(L, "first argument of separate must be table\n");
		return 0;
	}
	if(!lua_isnumber(L, 2)){
		on_error(L, "second argument of separate must be integer\n");
		return 0;
	}
		
	channels = lua_tointeger(L, 2);
	if(channels < 2){
		on_error(L, "second argument must be >= 2\n");
		return 0;
	}
	
	raw_len = luaL_getn(L, 1);
	
	if(raw_len % channels != 0){
		on_error(L, "the length of the array must be evenly divisible by %d\n", 
			channels);
		return 0;
	}

	chan_len = raw_len / channels;
	channel_data = (double*)calloc(sizeof(double), chan_len);
	
	for(i=0; i<channels; i++){
		memset(channel_data, 0, chan_len);
		for(j=0; j<chan_len; j++){
			lua_rawgeti(L, 1, j * channels + i + 1);
			channel_data[j] = lua_tointeger(L, -1);
			lua_pop(L, 1);
		}
		push_number_array(L, channel_data, chan_len);
	}
	
	free(channel_data);
	return channels;
}

int lua_read_sndfile(lua_State * L){
	if(lua_gettop(L) != 1){
		on_error(L, "read_sndfile takes exactly 1 argument\n");
		return 0;
	}
	if(!lua_isstring(L, 1)){
		on_error(L, "argument 1 of read_file must be a string\n");
		return 0;
	}
	
	const char * fname = lua_tostring(L, 1);
	int size;
	double * snddata = read_sndfile(fname, &size);
	
	if(snddata == NULL){
		on_error(L, "could not open file\n");
		return 0;
	}
	
	push_number_array(L, snddata, size);
	free(snddata);
	
	return 1;
}

int lua_write_sndfile(lua_State * L){
	if(lua_gettop(L) != 2){
		on_error(L, "write_sndfile takes exactly 2 arguments\n");
		return 0;
	}
	if(!lua_isstring(L, 1)){
		on_error(L, "argument 1 of write_sndfile must be a string\n");
		return 0;
	}
	if(!lua_istable(L, 2)){
		on_error(L, "argument 2 of write_sndfile must be a table\n");
		return 0;
	}
	
	const char * fname = lua_tostring(L, 1);
	int n = luaL_getn(L, 2);
	double * data = get_number_array(L, 2);
	int res;
	
	if(!write_sndfile(fname, data, n))
		on_error(L, "file write failed\n");

	return 0;
}

int lua_make_note(lua_State * L){
	double * array;
	int size;
	
	if(lua_gettop(L) != 1){
		on_error(L, "make_note takes one argument\n");
		return 0;
	}
	
	if(lua_isstring(L, 1)){
		const char * notestr = lua_tostring(L, 1);
		int len = lua_strlen(L, 1);
		char temp[len+1];
		strcpy(temp, notestr);
		array = make_notestr(temp, &size);
	} 
	else if(lua_istable(L, 1)){
		const char * note = get_string_field(L, 1, "note");
		int octave = (int)get_number_field(L, 1, "octave");
		double duration = get_number_field(L, 1, "beats");
		array = make_note(note, octave, duration, &size);
	}
	else {
		on_error(L, "argument 1 of make_note must be string or table\n");
		return 0;
	}
		
	push_number_array(L, array, size);
	free(array);
	
	return 1;
}

int lua_array_concat(lua_State * L){
	int i,j;
	int numargs, n;
	int total_size;
	int base = 0;
	double * array;
	
	numargs = lua_gettop(L);
	if(numargs < 2){
		on_error(L, "array_concat takes at least 2 arguments\n");
		return 0;
	}
		
	for(i=1; i<=numargs; i++)
		total_size += luaL_getn(L, i);
	
	array = (double*)calloc(sizeof(double), total_size);
	
	for(i=1; i<=numargs; i++){
		n = luaL_getn(L, i);
		for(j=1; j<=n; j++){
			lua_rawgeti(L, i, j);
			array[base+j-1] = lua_tonumber(L, -1);
			lua_pop(L, 1);
		}
		base += n;
	}
	
	push_number_array(L, array, total_size);
	free(array);
	
	return 1;
}
