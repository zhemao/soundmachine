#include <math.h>
#include "waveforms.h"
#include <stdlib.h>

static waveform all_waveforms[4] = {pure_sine, second_harmonic, 
									third_harmonic, clipped_wave};
static curwf = 0;

double pure_sine(double freq, double amp, double t){
	double theta = 2 * M_PI * freq * t;
	return amp * sin(theta);
}

double second_harmonic(double freq, double amp, double t){
	double theta1 = 2 * M_PI * freq * t;
	double theta2 = 2 * theta1;

	return (amp/2) * sin(theta1) + (amp/2) * sin(theta2);
}

double third_harmonic(double freq, double amp, double t){
	double theta[3];
	double res = 0;
	int i;

	theta[0] = 2 * M_PI * freq * t;
	theta[1] = 2 * theta[0];
	theta[2] = 2 * theta[1];

	res = (amp/2) * sin(theta[0]);
	res += (amp/4) * sin(theta[1]);
	res += (amp/4) * sin(theta[2]);

	return res;
}

double clipped_wave(double freq, double amp, double t){
	double theta = 2 * M_PI * freq * t;
	double res = amp * sin(theta);
	if(res < 1) return 0;
	return res - 1;
}

waveform string_to_wf(char * str){
	if(strcmp(str, "sin")==0){ curwf = 0; return pure_sine; }
	if(strcmp(str, "second")==0){ curwf = 1; return second_harmonic; }
	if(strcmp(str, "third")==0){ curwf = 2; return third_harmonic; }
	if(strcmp(str, "clip")==0){ curwf = 3; return clipped_wave; }
	
	return NULL;
}

waveform cycle_waveform(){
	curwf = (curwf + 1) % 4;
	return all_waveforms[curwf];
}
