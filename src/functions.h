#ifndef __SNDMACHINE_FUNCTIONS_H__
#define __SNDMACHINE_FUNCTIONS_H__

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

lua_State * globalState;
int exit_on_error;

void on_error (lua_State *L, const char *fmt, ...);

const char * get_string_field(lua_State * L, int ind, const char * key);
double get_number_field(lua_State * L, int ind, const char * key);
double * get_number_array(lua_State * L, int ind);
short * get_integer_array(lua_State * L, int ind);

void add_number(lua_State * L, const char * name, double d);
void add_integer(lua_State * L, const char * name, long i);
void add_string(lua_State * L, const char * name, char * str);
void add_cfunction(lua_State * L, const char * name, lua_CFunction func);
void push_integer_array(lua_State * L, short * array, int n);
void push_number_array(lua_State * L, double * array, int n);

int lua_set_waveform(lua_State * L);
int lua_set_tempo(lua_State * L);
int lua_set_amplitude(lua_State * L);
int lua_play(lua_State * L);
int lua_rest(lua_State * L);
int lua_play_freq(lua_State * L);
int lua_play_channels(lua_State * L);
int lua_play_raw(lua_State * L);
int lua_combine(lua_State * L);
int lua_mix(lua_State * L);
int lua_subtract(lua_State * L);
int lua_amplify(lua_State * L);
int lua_separate(lua_State * L);
int lua_read_sndfile(lua_State * L);
int lua_write_sndfile(lua_State * L);
int lua_make_note(lua_State * L);
int lua_array_concat(lua_State * L);
int lua_reset_device(lua_State * L);

#endif /* __SNDMACHINE_FUNCTIONS_H__ */

