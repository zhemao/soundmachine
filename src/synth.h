#ifndef __SNDMACHINE_SYNTH_H__
#define __SNDMACHINE_SYNTH_H__

#include <ao/ao.h>
#include "waveforms.h"

#define DEFAULT_CHANNELS 2
#define DEFAULT_SAMP_RATE 44100
#define DEFAULT_TEMPO 120

struct {
	ao_device * device;
	waveform wf;
	double amp;
	double tempo;
	long samp_rate;
	int channels;
} synth_state;

/* map notes to characters */
double get_freq(char * note, int octave);
int play_note(char * notestr, int octave, double beats);
int play_notestr(char * str);
void play_freq(double freq, double duration);
void beat_delay(double beats);
double * make_note(char * note, int octave, float beats, int * size);
double * make_notestr(char * str, int * size);
void reset_device(int driver, int channels, long samp_rate, char * filename);

#endif /* __SYNTH_H__ */



