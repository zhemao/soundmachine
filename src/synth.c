#include "synth.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#ifdef UNIX
	#include <unistd.h>
#endif

/* 
 * the frequencies of notes starting from C and going up by half notes 
 * for each octave 
 */ 
double frequencies[3][12] = {
	{130.81, 138.59, 146.83, 155.56, 164.81, 174.61, 185.00, 196.00, 207.65,
		220.00, 233.08, 246.94},
	{261.63, 277.18, 293.66, 311.13, 329.63, 349.23, 369.99,
		392.00, 415.30, 440.00, 466.16, 493.88},
	{523.25, 554.37, 587.33, 622.25, 659.26, 698.46, 739.99, 783.99, 830.61,
		880.00, 932.33, 987.77}
};
					
int note_to_index[7] = {9, 11, 0, 2, 4, 5, 7};

void reset_device(int driver, int channels, long samp_rate, char * filename){
	ao_sample_format format;
		
	if(synth_state.device != NULL)
		ao_close(synth_state.device);
	
	synth_state.channels = channels;
	synth_state.samp_rate = samp_rate;
	
	memset(&format, 0, sizeof(format));
	format.bits = sizeof(short)*8;
	format.channels = synth_state.channels;
	format.rate = synth_state.samp_rate;
	format.byte_format = AO_FMT_NATIVE;
	
	if(filename == NULL)
		synth_state.device = ao_open_live(driver, &format, NULL);
	else
		synth_state.device = ao_open_file(driver, filename, 1, &format, NULL);
}

double get_freq(char * note, int octave){
	int n = note_to_index[ note[0] - 97 ];

	if(note[1] == '#') n++;
	else if(note[1] == 'b') n--;

	if(n >= 12) n -= 12;
	else if(n < 0) n += 12;
	
	return frequencies[octave][n];
}

int play_note(char * note, int octave, double beats){
	float freq, dur;
	
	if(note[0] < 97 || note[0] > 103)
		return 0;
		
	if(octave > 2 || octave < 0)
		return 0;
	
	freq = get_freq(note, octave);
	dur = beats / synth_state.tempo * 60.0;
	
	play_freq(freq, dur);
	
	return 1;
}

int play_notestr(char * str){
	char * notestr;
	char * octstr;
	char * beatstr;
	
	notestr = strtok(str, "-");
	octstr = strtok(NULL, "-");
	beatstr = strtok(NULL, "-");
	
	if(notestr == NULL || octstr == NULL || beatstr == NULL)
		return 0;
	
	return play_note(notestr, atoi(octstr), atof(beatstr));
}

void play_freq(double freq, double duration){
	int buf_size = duration * synth_state.channels * synth_state.samp_rate;
	short buffer[buf_size];
	int i,j;
	
	for (i = 0; i < buf_size / synth_state.channels; i++){
		double t = (double)i/(double)synth_state.samp_rate;
		double samp = synth_state.wf(freq, synth_state.amp, t);
		for(j=0; j<synth_state.channels; j++){
			buffer[synth_state.channels*i+j] = (short)samp;
		}
	}
	
	ao_play(synth_state.device, (char*)buffer, buf_size*sizeof(short));
}

void beat_delay(double beats){
	double secs = beats / synth_state.tempo * 60.0;
	#ifdef UNIX
	usleep(secs * 1000000);
	#else
	int i;
	int buf_size = secs * synth_state.channels * synth_state.samp_rate;
	short buffer[buf_size];
	
	for(i=0; i<buf_size; i++){
		buffer[i] = 0;
	}
	
	ao_play(synth_state.device, (char*)buffer, buf_size*sizeof(short));
	#endif
}

double * make_note(char * note, int octave, float beats, int * size){
	int i;
	float freq, dur;
	double * buffer;
	int buf_size;
	
	freq = get_freq(note, octave);
	dur = beats / synth_state.tempo * 60.0;
	
	buf_size = dur * synth_state.samp_rate;
	buffer = calloc(sizeof(double), buf_size);
	
	for(i=0; i<buf_size; i++){
		double t = (double)i/(double)synth_state.samp_rate;
		double samp = synth_state.wf(freq, synth_state.amp, t);
		buffer[i] = samp;
	}
	
	*size = buf_size;
	
	return buffer;
}

double * make_notestr(char * str, int * size){
	char * notestr;
	char * octstr;
	char * beatstr;
	
	notestr = strtok(str, "-");
	octstr = strtok(NULL, "-");
	beatstr = strtok(NULL, "-");
	
	return make_note(notestr, atoi(octstr), atof(beatstr), size);
}

