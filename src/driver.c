#include "synth.h"
#include "waveforms.h"
#include "functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

void init_machine(lua_State * L, char * script){
	char * drivstr;
	char * wavestr;
	char * filename;
	int driver;
	int channels, samp_rate;
	
	if(luaL_loadfile(L, script) || lua_pcall(L, 0, 0, 0)){
		fprintf(stderr, "Error, could not load lua file %s\n", script);
		exit(EXIT_FAILURE);
	}
	
	lua_getglobal(L, "output_file");
	lua_getglobal(L, "channels");
	lua_getglobal(L, "samp_rate");
	lua_getglobal(L, "tempo");
	lua_getglobal(L, "amplitude");
	lua_getglobal(L, "driver");
	lua_getglobal(L, "waveform");
	
	if(lua_isstring(L, -7))
		filename = lua_tostring(L, -7);
	else filename = NULL;
	
	if(lua_isnumber(L, -6))
		channels = lua_tointeger(L, -6);
	else channels = DEFAULT_CHANNELS;
	
	if(lua_isnumber(L, -5))
		samp_rate = lua_tointeger(L, -5);
	else samp_rate = DEFAULT_SAMP_RATE;
	
	if(lua_isnumber(L, -4))
		synth_state.tempo = lua_tonumber(L, -4);
	else synth_state.tempo = DEFAULT_TEMPO;
	
	if(lua_isnumber(L, -3))
		synth_state.amp = lua_tonumber(L, -3);
	else synth_state.amp = SHRT_MAX;
	
	if(lua_isstring(L, -2))
		drivstr = lua_tostring(L, -2);
	else drivstr = "default";
	
	if(lua_isstring(L, -1))
		wavestr = lua_tostring(L, -1);
	else 
		wavestr = "sin";
	
	if(strcmp(drivstr, "default")==0){
		driver = ao_default_driver_id();
		if(driver == -1)
			on_error(L, "No default driver\n");
	}
	else {
		driver = ao_driver_id(drivstr);
		if(driver == -1)
			on_error(L, "No driver named '%s'\n", drivstr);
	}
	
	synth_state.wf = string_to_wf(wavestr);
	if(synth_state.wf == NULL) 
		on_error(L, "No waveform named '%s'\n", wavestr);
	
	reset_device(driver, channels, samp_rate, filename);
	
	if(synth_state.device == NULL)
		on_error(L, "Could not open device\n");
		
	lua_settop(L, 0);
}

void init_defaults(lua_State * L){
	int driver = ao_default_driver_id();
	
	synth_state.wf = string_to_wf("sin");
	synth_state.amp = SHRT_MAX;
	synth_state.tempo = DEFAULT_TEMPO;
	reset_device(driver, DEFAULT_CHANNELS, DEFAULT_SAMP_RATE, NULL);
	
	add_integer(L, "channels", DEFAULT_CHANNELS);
	add_integer(L, "samp_rate", DEFAULT_SAMP_RATE);
	add_number(L, "amplitude", SHRT_MAX);
	add_number(L, "tempo", DEFAULT_TEMPO);
	add_string(L, "waveform", "sin");
	add_string(L, "driver", "default");
}

void init_functions(lua_State * L){
	add_cfunction(L, "play", lua_play);
	add_cfunction(L, "set_waveform", lua_set_waveform);
	add_cfunction(L, "set_tempo", lua_set_tempo);
	add_cfunction(L, "set_amplitude", lua_set_amplitude);
	add_cfunction(L, "rest", lua_rest);
	add_cfunction(L, "play_freq", lua_play_freq);
	add_cfunction(L, "play_raw", lua_play_raw);
	add_cfunction(L, "play_channels", lua_play_channels);
	add_cfunction(L, "combine", lua_combine);
	add_cfunction(L, "mix", lua_mix);
	add_cfunction(L, "subtract", lua_subtract);
	add_cfunction(L, "amplify", lua_amplify);
	add_cfunction(L, "read_sndfile", lua_read_sndfile);
	add_cfunction(L, "write_sndfile", lua_write_sndfile);
	add_cfunction(L, "make_note", lua_make_note);
	add_cfunction(L, "array_concat", lua_array_concat);
	add_cfunction(L, "reset_device", lua_reset_device);
	add_cfunction(L, "separate", lua_separate);
}

void init_constants(lua_State * L){
	add_integer(L, "DEFAULT_CHANNELS", DEFAULT_CHANNELS);
	add_integer(L, "DEFAULT_SAMP_RATE", DEFAULT_SAMP_RATE);
	add_integer(L, "DEFAULT_TEMPO", DEFAULT_TEMPO);
	add_integer(L, "MAX_AMP", SHRT_MAX);
}

void start_repl(lua_State * L){
	char * line;

	while((line = linenoise("sndmach> ")) != NULL) {
		if (line[0] != '\0') {
			if(strcmp(line, "quit")==0 || strcmp(line, "exit")==0) break;
			linenoiseHistoryAdd(line);
			if(luaL_loadstring(L, line) || lua_pcall(L, 0, 0, 0)){
				const char * errmess = lua_tostring(L, -1);
				fprintf(stderr, "%s\n", errmess);
			}
		}
		free(line);
	}
}

int main(int argc, char *argv[]){
	int ch;
	lua_State *L;

	globalState = L = lua_open();
	luaL_openlibs(L);
	ao_initialize();
	
	init_functions(L);
	init_constants(L);
	init_defaults(L);
	
	if(argc < 2){
		exit_on_error = 0;
		start_repl(L);
	} else {
		exit_on_error = 1;
		if(luaL_loadfile(L, argv[1]) || lua_pcall(L, 0, 0, 0)){
			on_error(L, "Could not load lua script %s\n", argv[1]);
		}
	}
	
	lua_close(L);
	if(synth_state.device != NULL)
		ao_close(synth_state.device);
	ao_shutdown();
	return 0;
}
