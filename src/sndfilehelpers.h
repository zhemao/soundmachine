#ifndef __SNDFILE_HELPERS_H__
#define __SNDFILE_HELPERS_H__

int get_format(const char * fname);
double * read_sndfile(const char * fname, int * size);

#endif /* __SNDFILE_HELPERS_H__ */

