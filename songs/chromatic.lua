local notes = {'c','c#','d','d#','e'}
	
set_tempo(256)

for i,v in ipairs(notes) do
	play({note=v, octave=1, beats=1})
end
