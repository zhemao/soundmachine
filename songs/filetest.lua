local fname = "/usr/share/sounds/alsa/Front_Left.wav"
local snddata = read_sndfile(fname)
snddata = amplify(snddata, MAX_AMP)
play_channels(snddata, snddata)
local combined = combine(snddata, snddata)
play_raw(combined)
local channel1, channel2 = separate(combined, 2)
play_channels(channel1, channel2)

