function triplet()
	local trip = {'b', 'a', 'g'}
	for i,v in ipairs(trip) do
		play({note=v, octave=1, beats=1/3})
	end
end

function refrain()
	play('d-1-1', 'g-1-2')
	triplet()
	play('b-1-2')
end

function run()
	refrain()
	play('a-1-1', 'g-1-2', 'e-1-1', 'd-1-2')
	refrain()
	play('a-1-0.5', 'b-1-0.5', 'd-2-4')
	play('b-1-1', 'd-2-2')
	triplet()
	play('b-1-2', 'a-1-1', 'g-1-2', 'e-1-1', 'd-1-2')
	refrain()
	play('a-1-1', 'g-1-4')
end

run()
