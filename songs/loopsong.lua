function phrase1()
	local notes = {'c', 'eb', 'g'}
	for i=1,3 do
		local note = {note=notes[i], octave=1, beats=0.75}
		play(note)
		rest(0.25)
	end
end

function phrase2()
	rest(0.5)
	local notes = {'c', 'eb'}
	for i=1,2 do
		local note = {note=notes[i], octave=1, beats=0.75}
		play(note)
		rest(0.25)
	end
	play('g-1-0.5', 'g#-1-0.75')
	rest(0.25)
	play('g-1-0.75')
	rest(0.25)
end

function phrase3()
	rest(0.5)
	for i=1,2 do
		play('f-1-0.5')
		rest(0.5)
	end
	play('g-1-0.5', 'd-1-0.33', 'd#-1-0.33', 'd-1-0.83')
	rest(0.5)
end

function playall()
	phrase1()
	phrase2()
	phrase1()
	phrase3()
end

set_tempo(108)

while true do
	playall()
end
