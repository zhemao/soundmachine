function run()
	local buffer = {}
	local freq = 1000
	local amp = MAX_AMP
	for i=1,SAMP_RATE do
		local t = i/SAMP_RATE
		buffer[i] = amp * math.sin(2 * math.pi * freq * t)
	end 
	play_channels(buffer, buffer)
	local combined = combine(buffer, buffer)
	play_raw(combined)
	local channel2 = {}
	local freq2 = 2000
	for i=1,SAMP_RATE do
		local t = i/SAMP_RATE
		channel2[i] = amp * math.sin(2 * math.pi * freq2 * t)
	end 
	play_channels(buffer, channel2)
	local mixed = mix(buffer, channel2)
	play_channels(mixed, mixed)
	local subtracted = subtract(mixed, amplify(channel2, 0.5))
	play_channels(subtracted, subtracted)
end

run()
