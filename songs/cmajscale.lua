function run()
	play("c-1-1")
	local notes = {'d','e','f','g','a','b'}
	local octave = 1
	local beats = 0.5
	for i,v in ipairs(notes) do
		note = {note=v, octave=octave, beats=beats}
		play(note)
	end
	play("c-2-1")
	set_waveform("clip")
	local notes = {'b','a','g','f','e','d'}
	local raw = {}
	for i,v in ipairs(notes) do
		raw = array_concat(raw, make_note(v.."-1-0.5"))
	end
	play_channels(raw, raw)
	play("c-1-1")
end

run()
