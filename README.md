# SoundMachine - A scriptable music sampler/synthesizer

SoundMachine is a music sampler/synthesizer written in C using libao that can 
be scripted using the Lua programming language. It can be used for both 
high-level music generation and low-level manipulation of raw audio data.

## Dependencies

 * libao 1.1
 * lua 5.1 
 * libsndfile 1.0.25

Earlier version of libao and libsndfile may work. The versions given are simply
the ones I have installed. Lua must be 5.1 though, because of backward 
incompatibilities introduced into the Lua C API in 5.1. 

If you are using Debian, Ubuntu, or related distros, you can install the 
dependencies with.

	sudo apt-get install libsndfile1-dev libao-dev lua5.1 liblua5.1-0-dev 

On Red Hat, Fedora, CentOS and related distros, use

	yum install libsndfile libao lua

On ArchLinux and related distros, use
	
	pacman -S libsndfile libao lua
	
SoundMachine also uses the linenoise line-editing library written by Salvatore
Sanfilippo (AKA antirez) for the REPL. The linenoise code is included in this 
repo, you do not have to install it yourself.

## Installation

Once all dependencies are installed. Run make inside the src/ directory.
The `sndmach` executable will then be placed in the bin/ directory. Copy this
executable to somewhere on your path.

## Usage 

The command line usage is `sndmach [luafile]`, where luafile is the filename of 
a lua script. If the argument is given, SoundMachine will execute the script. 
You can find some example scripts in the songs/ directory. If called without 
arguments, SoundMachine will enter interactive mode and read in commands from 
the shell.

## Functions

SoundMachine exports the following functions into Lua.

### play(note1, ...)

This function takes a variable number of arguments. Each argument represents a 
single note. The note can either be a string or a table. If it is a string, it 
must correspond to this example: `"c-1-1"`, where `c` is the note to play, `1` 
is the octave, and `1` is the duration of the note. The note can be followed by 
`b` or `#` to play a flat or sharp, respectively. The octaves are numbered such 
that 1 is the octave containing middle C, and 0 and 2 are the octaves below and 
above that octave. The duration is given in beats. Therefore a duration of 1 
would play a quarter note, 2 would play a half note, 0.5 would play an eighth 
note, etc. The example, therefore, plays middle C for a quarter note duration. 
If the note is a table, it must have fields `note`, `octave` and `beats`, which 
correspond to the parts of the string mentioned previously. Note that `octave` 
and `beats` should be numbers, while `note` should be a string. 

### play_freq(freq, duration)

Play a note at the given frequency for the given duration **in seconds**. 

### play_raw(rawdata)

Play raw samples given in Lua arrays. It takes a single argument which should be
an array containing the raw audio data. If there are multiple channels, the 
samples for the channels should alternate in the array. So, the first item would 
be for channel 1, the second for channel 2, the third for channel 1, and so on.

### play_channels(channel1, channel2, ...)

Similar to play_raw except the samples for each channel are in separate arrays.
The number of arguments play_raw takes  depends on the number of audio channels 
on your system. By default, SoundMachine is compiled assuming two channels. 
The arrays must all be the same length. 

### rest(beats)

Delay playback for a given number of beats.

### make_note(note)

Generates the raw audio data from a note description. Takes a single argument 
which can be a string or a table, similar to the arguments to `play`. Returns 
an array containing the raw audio data. Note: the audio data returned is 
**single channel**. 

### array_concat(array1, array2, ...)

Utility function to concatenate several numerical arrays together. Note: all
array elements must be numbers.

### read_sndfile(filename)

Read in an audio file with the given name and return its audio data in the form
of a lua array. The returned array is suitable for passing into `play_raw`.
The file type will be automatically detected. This function can read any audio
format supported by [libsndfile](http://www.mega-nerd.com/libsndfile/).

### combine(channel1, channel2, ...)

Interleaves the samples from two or more channels into one array. The returned
value is suitable for being passed into play_raw. Array arguments must be of 
the same length.

### separate(snddate, channels)

Does the opposite of `combine`. Separates raw audio data into the given number 
of channels. Returns the separated channels as multiple arrays. 

### mix(channel1, channel2, ...)

Mixes the data from multiple channels together into one channel. Each element
of the returned array will be the average of the corresponding elements of the
argument arrays. Array arguments must be of the same length.

### subtract(channel1, channel2)

Subtract the samples from channel2 out of channel1. Array arguments must be of 
the same length.

### set_waveform(wf)

Sets the waveform formula that play and play_freq use to generate audio data.
Possible values are:

 * `"sin"` - pure sine wave
 * `"second"` - sine wave w/ second harmonic
 * `"third"` - sine wave w/ second and third harmonics
 * `"clip"` - clipped sine wave

### set_tempo(tempo)

Set the tempo of the synthesizer in beats per minute. 

### set_amplitude(amp)

Set the amplitude of the synthesizer

### reset_device([driver], [channels], [sample_rate], [output_file])

Reset the audio output device. The description of the arguments are as follows

 * **driver** - A string indicating the libao driver that SoundMachine will use. 
 This can either be `"default"`, in which case libao will use the default sound 
 driver on your system, or any of the drivers listed in the 
 [libao documentation](http://www.xiph.org/ao/doc/drivers.html). 
 If this variable is not defined, the default driver will be used.
 * **channels** - The number of channels that the audio device will use. 
 Defaults to 2.
 * **sample_rate** - The sample rate used by the audio device.
 * **output_file** - The file to which audio data will be output. This should 
 only be used if driver is `"au"`, `"raw"`, or `"wav"`.

## Constants

The following constants are exported by SoundMachine. 

 * DEFAULT_CHANNELS - the default number of channels of the audio output device (2)
 * DEFAULT_SAMP_RATE - the default sample rate of the audio output device (44100)
 * DEFAULT_TEMPO - the default tempo in beats per minute (120)
 * MAX_AMP - the maximum amplitude possible. You must not set the amplitude to
   anything greater than this value. 

## Licensing

Soundmachine is released under a BSD License, reproduced in LICENSE.
